
from flask import Flask
from flask_restful import Api
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config
from db_init import populate
import routes

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)

api.add_resource(routes.PessoaListAPI, '/pessoas/')
api.add_resource(routes.ContaListAPI, '/contas/')
api.add_resource(routes.TransacaoListAPI, '/transacoes/')

api.add_resource(routes.PessoaAPI, '/pessoas/int:id')
api.add_resource(routes.ContaAPI, '/contas/int:id')
api.add_resource(routes.TransacaoAPI, '/transacoes/int:id')

if __name__ == '__main__':
    db.create_all()
    migrate.init_app(app)
    migrate.upgrade()
    populate(db)
    app.run()

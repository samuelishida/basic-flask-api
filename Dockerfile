FROM jalll/python3.7-mysql-pillow

WORKDIR /app

COPY reqs.txt .
COPY app.py .
COPY db_init.py .
COPY config.py .
COPY routes.py .
COPY models.py .
COPY run.sh .
RUN chmod +x run.sh

RUN pip install -r reqs.txt

ENV FLASK_APP=app.py

EXPOSE 5000

CMD [ "./run.sh" ]


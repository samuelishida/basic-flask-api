from flask_restful import Resource
from flask_restful import marshal_with
from flask import abort
from models import Conta, Pessoa, Transacao, conta_fields, pessoa_fields, transacao_fields

class PessoaListAPI(Resource):
    @marshal_with(pessoa_fields)
    def get(self):
        pessoas = Pessoa.query.all()
        return pessoas

class ContaListAPI(Resource):
    @marshal_with(conta_fields)
    def get(self):
        contas = Conta.query.all()
        return contas

class TransacaoListAPI(Resource):
    @marshal_with(transacao_fields)
    def get(self):
        transacoes = Transacao.query.all()
        return transacoes

class PessoaAPI(Resource):
    @marshal_with(pessoa_fields)
    def get(self, id):
        pessoa = Pessoa.query.get(id)
        if not pessoa:
            abort(404, message="Pessoa {} doesn't exist".format(id))
        return pessoa

class ContaAPI(Resource):
    @marshal_with(conta_fields)
    def get(self, id):
        conta = Conta.query.get(id)
        if not conta:
            abort(404, message="Conta {} doesn't exist".format(id))
        return conta

class TransacaoAPI(Resource):
    @marshal_with(transacao_fields)
    def get(self, id):
        transacao = Transacao.query.get(id)
        if not transacao:
            abort(404, message="Transacao {} doesn't exist".format(id))
        return transacao

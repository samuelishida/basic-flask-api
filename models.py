from flask_sqlalchemy import SQLAlchemy
from flask_restful import fields, marshal

db = SQLAlchemy()


class Pessoa(db.Model):
    idPessoa = db.Column(db.Integer, primary_key=True)
    
    cpf = db.Column(db.String(11), nullable=False)
    nome = db.Column(db.String(50), nullable=False)

    dataNascimento = db.Column(db.Date, nullable=False)


    def __init__(self, nome, cpf, dataNascimento):
        self.nome = nome
        self.cpf = cpf
        self.dataNascimento = dataNascimento
    
    def to_dict(self):
        return marshal(self, pessoa_fields)
    

class Conta(db.Model):
    idConta = db.Column(db.Integer, primary_key=True)
    idPessoa = db.Column(db.Integer, db.ForeignKey('pessoa.idPessoa'), nullable=False)

    saldo = db.Column(db.Float, nullable=False)
    tipoConta = db.Column(db.Integer, nullable=False)
    flagAtivo = db.Column(db.Boolean, nullable=False)
    limiteSaqueDiario = db.Column(db.Float, nullable=False)

    dataCriacao = db.Column(db.Date, nullable=False)


    def __init__(self, idPessoa, saldo, limiteSaqueDiario, flagAtivo, tipoConta, dataCriacao):
        self.idPessoa = idPessoa
        self.saldo = saldo
        self.tipoConta = tipoConta
        self.flagAtivo = flagAtivo
        self.limiteSaqueDiario = limiteSaqueDiario
        self.dataCriacao = dataCriacao
    
    def to_dict(self):
        return marshal(self, conta_fields)

class Transacao(db.Model):
    idTransacao = db.Column(db.Integer, primary_key=True)
    idConta = db.Column(db.Integer, db.ForeignKey('conta.idConta'), nullable=False)

    valor = db.Column(db.Float, nullable=False)

    dataTransacao = db.Column(db.Date, nullable=False)


    def __init__(self, idConta, valor, dataTransacao):
        self.idConta = idConta
        self.valor = valor
        self.dataTransacao = dataTransacao
    
    def to_dict(self):
        return marshal(self, transacao_fields)



pessoa_fields = {
    'idPessoa': fields.Integer,
    'cpf': fields.String,
    'nome': fields.String,
    'dataNascimento': fields.DateTime
}

conta_fields = {
    'idConta': fields.Integer,
    'idPessoa': fields.Integer,
    'saldo': fields.Float,
    'tipoConta': fields.Integer,
    'flagAtivo': fields.Boolean,
    'limiteSaqueDiario': fields.Float,
    'dataCriacao': fields.DateTime
}

transacao_fields = {
    'idTransacao': fields.Integer,
    'idConta': fields.Integer,
    'valor': fields.Float,
    'dataTransacao': fields.DateTime
}